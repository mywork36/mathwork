function isEven(number) {
  if (number % 2 == 0) {
    console.log(`${number} is Even`)
  } else {
    console.log(`${number} is not Even`)
  }
}

isEven(10)

isEven(11)

function isOdd(number) {
  if (number % 2 == 0) {
    console.log(`${number} is Even`)
  } else {
    console.log(`${number} is Odd`)
  }
}

isOdd(10)

isOdd(11)

function add(p1, p2) {
  console.log(`${p1}+${p2}=${p1 + p2}`)
}

add(10, 12)

add(11, 12)

function sub(p1, p2) {
  console.log(`${p1}-${p2}=${p1 - p2}`)
}

sub(10, 5)

sub(11, 5)
